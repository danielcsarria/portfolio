import React, { Component } from 'react';
import ModIntro from './modules/ModIntro';
import initiateDate from './data';

export default class Introduction extends Component {

  state = initiateDate;

  render() {

    const modules = this.state.ModIntro;

    return (
      <div>
        <section id="colorlib-hero" className="js-fullheight" data-section="home">
          <div className="flexslider js-fullheight">
            <ul className="slides">

              {
                Object.keys(modules).map((moduleKey) => {
                  return <ModIntro
                  module = {modules}
                  moduleKey = {moduleKey}
                  key = {moduleKey}

                  >
                  </ModIntro>
                })
              }
            
            </ul>
          </div>
        </section>
      </div>
    )
  }
}
