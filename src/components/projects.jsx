import React, { Component } from 'react';
import ModProj from './modules/ModProj';
import initiateDate from './data';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';

export default class Projects extends Component {
  
	state = initiateDate;

	projTitle = this.state.ModProj[0].title;
	projImg = this.state.ModProj[0].image;
	projLink = this.state.ModProj[0].link;
	projDesc = this.state.ModProj[0].desc;


	onDragEnd = result =>  {

		const modules = this.state.ModProj;
		const modKeys = Object.keys(modules);

		const { destination, source, draggableId} = result;

		if (
			destination.droppableId === source.droppableId &&
			destination.index === source.index
		) {
			console.log("nothing moved")
			return;
		}

		const selected = source.index;
		const target = destination.index;

		// console.log("selected =>", selected)
		// console.log("target =>", target)
		console.log("modules =>", modules)

		var selectedItem = modules.splice(selected, 1);
		modules.splice(target, 0, selectedItem[0]);


		this.projTitle = this.state.ModProj[0].title;
		this.projImage = this.state.ModProj[0].image;
		this.projLink = this.state.ModProj[0].link;
		this.projDesc = this.state.ModProj[0].desc;

		this.setState(this.state);
	}
	
	render() {

		const projImage = this.state.ModProj[0].image;
		const modules = this.state.ModProj;
		const image = "images/" + projImage;

		console.log("projImg =>", this.projImg)

		return (
			<DragDropContext onDragEnd={this.onDragEnd}>
				<div>
					<section className="colorlib-work" data-section="projects">
						<div className="colorlib-narrow-content">
							<div className="row">
								<div className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
									<h2 className="colorlib-heading animate-box">Recent Projects</h2>
									<span className="heading-meta">Drag an image to the top to learn more!</span>
								</div>
							</div>
							
							<div className="row flex">
							
								<Droppable
									droppableId="droppable" direction="vertical"
								>
									{(provided) => (
										<div className="row ddcont-left"
											{...provided.droppableProps}
											ref={provided.innerRef}
										>

											{
												modules.map((modules, moduleIndex) => {
													return <ModProj
													moduleIndex = {moduleIndex}
													module = {this.state.ModProj}
													moduleKey = {modules}
													key = {moduleIndex}

													>
													</ModProj>
												})
											}
											{provided.placeholder}

										</div>
									)}
								</Droppable>

								<div className="row ddcont-right">
								
									<div className="project-container">
									
										<h3>{this.projTitle}</h3>
										<h5><a href={this.projLink} target="_blank">{this.projLink}</a></h5>
										<div className="project-image">
										
											<img src={image} alt=""/>

										</div>
										<p>
											{this.projDesc}
										</p>
									
									</div>
								
								</div>
							
							</div>				
						
						</div>
					</section>
				</div>
			</DragDropContext>
		)
  	}
}
