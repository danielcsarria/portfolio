import React, { Component } from 'react'
import ModAbout from './modules/M-About';
import initiateDate from './data';


export default class About extends Component {

    state = initiateDate;
    

  render() {
    // console.log(this.state.modules.Module1);
    const modules = this.state.ModulesAbout
    return (
      <div>
        <section className="colorlib-about" data-section="about">
        <div className="colorlib-narrow-content">
            <div className="row">
            <div className="col-md-12">
                <div className="row row-bottom-padded-sm animate-box" data-animate-effect="fadeInLeft">
                <div className="col-md-12">
                    <div className="about-desc">
                    {/* <span className="heading-meta">About Us</span> */}
                    <h2 className="colorlib-heading">Who Am I?</h2>
                    <p>I am a passionate and hard working Web Developer</p>
                    <p>I am expert Wordpress theme developer and can make any static HTML website into a fully dynamic and functional Wordpress theme with a healthy balance between code and plugins. <br/>

                    I am also proficient in HTML5, Bootstrap4, CSS3, SCSS, javascript, Jquery, React.js, PHP, and mySQL. <br/> <br/>

                    If you would like to know more, please email me and ask me for my resume!</p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section>
        <section className="colorlib-about">
        <div className="colorlib-narrow-content">
            <div className="row">
                <div className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                    <span className="heading-meta">What I do</span>
                </div>
            </div>

            <div className="row row-pt-md">

                {
                    Object.keys(modules).map((moduleKey) => {
                        return <ModAbout
                            modules = {modules}
                            moduleKey = {moduleKey}
                            key = {moduleKey}
                        >
                        </ModAbout>
                    })
                }

            </div>
        </div>
        </section>
      </div>
    )
  }
}
