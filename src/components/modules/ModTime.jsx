import React, { Component } from 'react'

export default class ModAbout extends Component {

    render () {

        const moduleKey = this.props.moduleKey;
        const module = this.props.module[moduleKey];

        const color = "timeline-icon " + module.color;

        return (
            <article className="timeline-entry animate-box" data-animate-effect="fadeInLeft">
                <div className="timeline-entry-inner">
                    <div className={color}>
                        <i className="icon-pen2" />
                    </div>
                    <div className="timeline-label">
                        <h2>{module.title} <span> {module.date}</span></h2>
                        <p>{module.content}</p>
                    </div>
                </div>
            </article>
        )
    }
}