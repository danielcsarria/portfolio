import React, { Component } from 'react'

export default class ModAbout extends Component {

    render () {
        const moduleKey = this.props.moduleKey;
        const module = this.props.modules[moduleKey];

        const color = "services " + module.color;
        const title = module.title;
        const content = module.content;
        const icon = module.icon;

        return (
            <div className="col-md-4 text-center animate-box">
                <div className={color}>
                <span className="icon">
                    <i className={icon} />
                </span>
                <div className="desc">
                    <h3>{title} </h3>
                    <p>{content}</p>
                </div>
                </div>
            </div>
        )
    }
}