import React, { Component } from 'react'
import { Draggable } from 'react-beautiful-dnd';


export default class ModProj extends Component {

    render () {
        const moduleKey = this.props.moduleKey;
        const moduleIndex = this.props.moduleIndex
        const module = this.props.module[moduleIndex];
        const draggableId = String(moduleIndex);

        const img = "url(images/" + module.image + ")";


        return ( 
            <Draggable
                draggableId={draggableId}
                index = {moduleIndex}
            >
                {(provided) => (
                    <div
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        ref = {provided.innerRef}
                        className="col-md-12 proj-item" 
                        
                    >
                        <div className="project" style={{backgroundImage: img}}>
                            <div className="desc">
                                <div className="con">
                                    <h3><a href="www.google.com">{module.title}</a></h3>
                                    <span>{module.type}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </Draggable>
        )
    }
}


