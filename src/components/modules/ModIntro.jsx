import React, { Component } from 'react'

export default class ModAbout extends Component {

    

    render () {

        const moduleKey = this.props.moduleKey;
        const module = this.props.module[moduleKey];

        const image = "url(images/" + module.image + ")";

        const style = {
            backgroundImage : image,
            height: "100vh",
            backgroundAttachment: "fixed",
            backgroundPosition : "center",
            backgroundRepeat : "no-repeat",
            backgroundSize : "cover",
        }

        function button() {
            if(module.button.case === true) {
                return (
                    <p><a className="btn btn-primary btn-learn" href={module.button.link} target="_blank" rel="noopener noreferrer">{module.button.label} <i className={module.button.icon} /></a></p>
                )
            }
        }

        return (
            <li style={style}>
            <div className="overlay" />
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3 col-md-pull-3 col-sm-12 col-xs-12 js-fullheight slider-text">
                            <div className="slider-text-inner js-fullheight">
                                <div className="desc">
                                    <h1>{module.title}</h1>
                                    {button()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}