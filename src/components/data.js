const initiateDate = {
    ModulesAbout: {
        "Module1": {
            "color": "color-1",
            "icon": "icon-html-five2",
            "title": "Custom Web Solutions",
            "content": "Full custom website solutions using HTML5, CSS3/SCSS/SASS with beautifully laid out code for optimal customization"
        },
        "Module2": {
            "color": "color-2",
            "icon": "icon-wordpress",
            "title": "Wordpress",
            "content": "Expert Wordpress theme development using PHP making every aspect of a website completely dynamic and easy to use for the end consumer"
        },
        "icon-phone3": {
            "color": "color-3",
            "icon": "icon-phone3",
            "title": "Mobile",
            "content": "Mobile optimization to make sure that everything looks great. From the biggest monitors, to the smallest smartphone screens"
        },
    },
    ModTime : {
        "Module1" : {
            "title" : "Internship at Juniper",
            "date"  : "2019 - Present",
            "content" : "I recently have joined the Juniper Networs as a Software Developer-Intern in the DevOps team. My major part of the work has been into the field of creating sustainable and flexible CI/CD (Continuous Integration & Continuous Development) pipelines which enables developers at Juniper to do rapid development. I am also exploring the role of container technology like Docker and container-orchrastation technologies like K8s, Swarm to create industry standard production environments.",
            "color" : "color-3",
        },
        "Module2" : {
            "title" : "Undergraduation at DAIICT",
            "date"  : "2015-2019",
            "content" : "I am pursuing my under-graduation studies with major in I.C.T.(Information & Communication Technology). I have taken courses like DSA, OOPs, Computer Networks, IOT, Web-Data Management over the years and have better understanding of these subjects.I have also been part of S.B.G.(Student Body Govenance) scince my first year of college.",
            "color" : "color-4",
        },
        "Module3" : {
            "title" : "Primary and Higher Education",
            "date"  : "2003-2015",
            "content" : "I have completed my higher secondary education with major subjects as Physics,Chemistry & Maths with 91 merit. During my time at school, I have developed interest in solving complex problems of the fundamental physics which helped me to improve my understanding of any problem and also my mathematical skills to actually solve the problems.",
            "color" : "color-5",
        }
    },
    ModProj : [
        {
            "title" : "Hastings Architecure",
            "type" : "Custom hand coded Wordpress theme with PHP",
            "image" : "hastings.jpg",
            "link" : "https://github.com/danielcsarria/dnd-tsi",
            "desc" : "Strata of cigarette smoke rose from the tiers, drifting until it struck currents set up by the blowers and the dripping chassis of a broken mirror bent and elongated as they fell. She peered at the clinic, Molly took him to the Tank War, mouth touched with hot gold as a gliding cursor struck sparks from the wall of a painted jungle of rainbow foliage.",
        },
        {
            "title" : "TownSquare Interactive Layout Editor",
            "type" : "React.js application for internal CMS",
            "image" : "tsidnd.jpg",
            "link" : "https://www.facebook.com",
            "desc" : "Strata of cigarette smoke rose from the tiers, drifting until it struck currents set up by the blowers and the dripping chassis of a broken mirror bent and elongated as they fell. She peered at the clinic, Molly took him to the Tank War, mouth touched with hot gold as a gliding cursor struck sparks from the wall of a painted jungle of rainbow foliage.",
            
        },
        {
            "title" : "Willow Street Pub",
            "type" : "Custom hand coded Wordpress theme with PHP",
            "image" : "willow.jpg",
            "link" : "http://willowstpub.com/",
            "desc" : "Strata of cigarette smoke rose from the tiers, drifting until it struck currents set up by the blowers and the dripping chassis of a broken mirror bent and elongated as they fell. She peered at the clinic, Molly took him to the Tank War, mouth touched with hot gold as a gliding cursor struck sparks from the wall of a painted jungle of rainbow foliage.",
        },
        {
            "title" : "Lisa Herman Massage",
            "type" : "Custom hand coded Wordpress theme with PHP",
            "image" : "lisa.jpg",
            "link" : "http://lisahmassage.com/",
            "desc" : "Strata of cigarette smoke rose from the tiers, drifting until it struck currents set up by the blowers and the dripping chassis of a broken mirror bent and elongated as they fell. She peered at the clinic, Molly took him to the Tank War, mouth touched with hot gold as a gliding cursor struck sparks from the wall of a painted jungle of rainbow foliage.",
        },
    ],

    ModIntro : {
        "Module1" : {
            "image" : "img_bg.jpg",
            "title" : "",
            "button" : {
                "case" : false,
                "label" : "View whatever",
                "icon" : "icon-download4",
                "link" : "https://github.com/danielcsarria"
            },
        },
    },
    Options : {
        "sidebar" : {
            "name" : "Daniel Sarria",
            "profile" : "about.jpg",
            "email" : "danielcsarria@gmail.com",
            "phone" : "267-264-8751", 
            "instagram" : "https://www.instagram.com/danny_s14usmc/",
            "linkedin" : "https://www.linkedin.com/in/daniel-sarria/",
            "nav" : {
                "introduction" : true,
                "about" : true,
                "timeline" : true,
                "projects" : true,
            }
        },
    }
};

export default initiateDate;