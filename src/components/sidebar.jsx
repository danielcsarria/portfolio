import React, { Component } from 'react'
import initiateDate from './data';

export default class Sidebar extends Component {

  state = initiateDate;

  render() {

    const options = this.state.Options.sidebar;
    const image = "url(images/" + options.profile + ")";

    return (
      <div>
        <div>
          <nav href="#navbar" className="js-colorlib-nav-toggle colorlib-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i /></nav>
          <aside id="colorlib-aside" className="border js-fullheight">
            <div className="text-center">
              <div className="author-img" style={{backgroundImage: image}} />
              <h1 id="colorlib-logo"><a href="index.html">{options.name}</a></h1>
              <span className="email"><i className="icon-mail"></i>{options.email}</span>
              <span className="number"><i className="icon-phone"></i>{options.phone}</span>
            </div>
            <nav id="colorlib-main-menu" role="navigation" className="navbar">
              <div id="navbar" className="collapse">
                <ul>
                  <li className="active"><a href="#home" data-nav-section="home">Introduction</a></li>
                  <li><a href="#about" data-nav-section="about">About</a></li>
                  <li><a href="#" data-nav-section="projects">Projects</a></li>
                  <li><a href="#timeline" data-nav-section="timeline">Timeline</a></li>
                </ul>
              </div>
            </nav>
            <nav id="colorlib-main-menu">
              <ul>
                {/* <li><a href="https://www.facebook.com/dhruv.barochia" target="_blank" rel="noopener noreferrer"><i className="icon-facebook2" /></a></li> */}
                {/* <li><a href="https://twitter.com/ddbarochiya" target="_blank" rel="noopener noreferrer"><i className="icon-twitter2" /></a></li> */}
                <li><a href={options.instagram} target="_blank" rel="noopener noreferrer"><i className="icon-instagram" /></a></li>
                <li><a href={options.linkedin} target="_blank" rel="noopener noreferrer"><i className="icon-linkedin2" /></a></li>
                {/* <li><a href="https://github.com/Dhruv34788" target="_blank" rel="noopener noreferrer"><i className="icon-github"></i></a></li> */}
                {/* <li><a href="https://medium.com/@dhruv.barochia34788" target="_blank" rel="noopener noreferrer"><i className="icon-blogger2"></i></a></li> */}
              </ul>
            </nav>
            <div className="colorlib-footer">
              <p><small>
                  Powered by React.js
              </small></p>
            </div>
          </aside>
        </div>
      </div>
    )
  }
}
